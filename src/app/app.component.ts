import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, catchError, map, Observable, of, startWith} from "rxjs";
import {ApiResponse} from "./domain/api-response";
import {Page} from "./domain/page";
import {HttpErrorResponse} from "@angular/common/http";
import {ComicService} from "./service/comic.service";
import {ComicDto} from "./domain/comic-dto";
import {NgForm} from "@angular/forms";
import {getSupportedBrowsers} from "@angular-devkit/build-angular/src/utils/supported-browsers";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  comicsState$: Observable<{ appState: string, appData?: ApiResponse<Page>, error?: HttpErrorResponse }>
  responseSubject :BehaviorSubject<ApiResponse<Page>> = new BehaviorSubject<ApiResponse<Page>>(null);
  private currentPageSubject: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  currentPage$ :Observable<number> = this.currentPageSubject.asObservable();
  public updateComic: ComicDto;
  public deleteComic: ComicDto;
  constructor(private comicService: ComicService) {
  }

  ngOnInit(): void {
    this.initView();
  }

  initView(): void{
    this.comicsState$ = this.comicService.comicsPages$().pipe(
      map((response: ApiResponse<Page>) => {
          this.responseSubject.next(response);
          this.currentPageSubject.next(response.data.number);
          console.log(response);
          return ({appState: "APP_LOADED", appData: response});
        }
      ),
      startWith({appState: "APP_LOADING"}),
      catchError((error: HttpErrorResponse) => of({appState: 'APP_ERROR', error}))
    )

  }

  /**
   * Method used to go until the desire page
   */
  goToPage(name?: string, pageNumber: number = 0): void {
    this.comicsState$ = this.comicService.comicsPages$(name, pageNumber).pipe(
      map((response: ApiResponse<Page>) => {
        this.responseSubject.next(response);
        this.currentPageSubject.next(response.data.number);
          console.log(response);
          return ({appState: "APP_LOADED", appData: response});
        }
      ),
      startWith({appState: "APP_LOADED", appData: this.responseSubject.value}),
      catchError((error: HttpErrorResponse) => of({appState: 'APP_ERROR', error}))
    )
    }
  goToNextOrPreviusPage(direction?: string, name?: string):void {
    this.goToPage(name, direction === 'forward' ? this.currentPageSubject.value+1 : this.currentPageSubject.value-1)
  }

  /**
   * this method do something like this, note that data-bs-target is dynamically, so we dont set here
   * <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
   *   Launch demo modal
   * </button>
   * @param comicDto
   * @param mode
   */
  public onOpenModal(comicDto: ComicDto, mode: string): void {
    const button:HTMLButtonElement = document.createElement('button');
    // get the container div created with id table-container
    const container: HTMLElement = document.getElementById("table-container");
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-bs-toggle', 'modal');
    switch (mode) {
      case 'create': {
        button.setAttribute('data-bs-target', '#createComicModal');
        break;
      }
      case 'update': {
        this.updateComic = comicDto;
        button.setAttribute('data-bs-target', '#updateComicModal');
        break;
      }
      default:{
        this.deleteComic = comicDto;
        button.setAttribute('data-bs-target', '#deleteComicModal');
        break;
      }
    }
    container.appendChild(button);
    button.click();
  }

  /**
   * Method used to create the comic, is used by the form
   * @param comicDto
   */
  onCreateComic(comicDto :NgForm): void{
    document.getElementById('create-comic-form').click();
    this.comicService.createComic(comicDto.value).subscribe(
      (response: ComicDto) => {
        this.initView();
        console.log(response);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  /**
   * Method used to update a comic, is used by the form
   * @param comicDto
   */
  onUpdateComic(comicDto: ComicDto): void{
    document.getElementById('update-comic-form').click();
    this.comicService.updateComic(comicDto).subscribe(
      (response: ComicDto) => {
        this.initView();
        console.log(response);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  /**
   * Method used to update a comic, is used by the form
   * @param comicId
   */
  onDeleteComic(comicId: number): void{
    document.getElementById('delete-comic-form').click();
    this.comicService.deleteComic(comicId).subscribe(
      (response: void) => {
        this.initView();
        console.log(response);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
}
