import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ApiResponse} from "../domain/api-response";
import {Page} from "../domain/page";
import {ComicDto} from "../domain/comic-dto";
import {NgForm} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class ComicService {

  private readonly serverUrl: string = 'http://localhost:8081/tiendaComic';

  constructor(private httpClient: HttpClient) { }

  // Make call to the API to retrieve page of comics
  //getComicsPages(name: string = '', page: number = 0, size: number = 10): Observable<ApiResponse<Page>>{
  //  return this.httpClient
  //    .get<any>(`${this.serverUrl}/getComicsPages?name=${name}&page=${page}&size=${size}`)
  //}

  //Same thing with observables
  comicsPages$ = (name: string = '', page: number = 0, size: number = 5): Observable<ApiResponse<Page>> =>
    this.httpClient
      .get<any>(`${this.serverUrl}/getComicsPages?name=${name}&page=${page}&size=${size}`)

  // Method to create a comic
  public createComic(comicDto :ComicDto): Observable<ComicDto> {
    return this.httpClient.post<any>(`${this.serverUrl}/createComic`, comicDto);
  }

  // Method to get a comic with an id
  public getComic(id: number): Observable<ComicDto> {
    return this.httpClient.get<any>(`${this.serverUrl}/getComic/${id}`);
  }

  // Method to update a comic
  public updateComic(comicDto: ComicDto): Observable<ComicDto> {
    return this.httpClient.patch<any>(`${this.serverUrl}/updateComic`, comicDto);
  }
  // Method to delete a comic
  public deleteComic(id: number): Observable<void> {
    return this.httpClient.delete<any>(`${this.serverUrl}/deleteComic/${id}`);
  }
}
