import {TematicaEnum} from "./tematica-enum";
import {EstadoEnum} from "./estado-enum";

export interface ComicDto {
  id: number;
  nombre: string;
  editorial: string;
  tematicaEnum: TematicaEnum;
  coleccion: string;
  numeroPaginas: number;
  precio: number;
  autores: string;
  color: boolean;
  cantidad: number;
  fechaVenta: Date;
  estadoEnum: EstadoEnum;
}
